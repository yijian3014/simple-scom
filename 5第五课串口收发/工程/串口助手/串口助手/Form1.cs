﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;

namespace 串口助手
{
    public partial class Form1 : Form
    {
        private bool isOpen = false;
        //SerialPort
        public Form1()
        {
            InitializeComponent();

            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //在设计页面已经预先添加了COM1 COM2 COM3 
            //this.port_cbb.Items.Add("COM4");
            //this.port_cbb.Items.Add("COM5");

            //this.port_cbb.SelectedIndex = 2;

            //this.baud_cbb.SelectedIndex = 1;

            //this.check_cbb.SelectedIndex = 0;

            //this.databit_cbb.SelectedIndex = 3;

            //this.stopbit_cbb.SelectedIndex = 0;

            // 给发送框添加预设文本
            //  this.send_rtb.Text = "我是发送框";

            // 给接收框添加预设文本
            //  this.recive_rtb.Text = "我是接收框";

            serialLoad();
        }

        private void serialLoad()
        {
            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey(@"Hardware\DeviceMap\SerialComm");

            string[] sSubKeys = keyCom.GetValueNames();
            port_cbb.Items.Clear();
            foreach (var sValue in   sSubKeys)
            {
                string portName = (string)keyCom.GetValue(sValue);
                port_cbb.Items.Add(portName);
            }

            this.port_cbb.SelectedIndex = 0;

            this.baud_cbb.SelectedIndex = 1;

            this.check_cbb.SelectedIndex = 0;

            this.databit_cbb.SelectedIndex = 3;

            this.stopbit_cbb.SelectedIndex = 0;
        }



        private void send_btn_Click(object sender, EventArgs e)
        {
            // 如果发送的数据不为空，则接收
            if (this.send_rtb.Text != "" && serialPort1.IsOpen)
            {
                //this.recive_rtb.AppendText(this.send_rtb.Text);
                serialPort1.Write(send_rtb.Text);
            }
            else
            {
                MessageBox.Show("请先输入发送数据！");

            }
        }

        private void open_btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (serialPort1.IsOpen == false)
                {
                    serialPort1.PortName = port_cbb.Text;
                    serialPort1.BaudRate = Convert.ToInt32(baud_cbb.Text);
                    serialPort1.DataBits = Convert.ToInt32(databit_cbb.Text);
                    switch (check_cbb.SelectedIndex)
                    {
                        //  none  odd  even 
                        case 0:
                            serialPort1.Parity = Parity.None;
                            break;
                        case 1:
                            serialPort1.Parity = Parity.Odd;
                            break;
                        case 2:
                            serialPort1.Parity = Parity.Even;
                            break;
                        default:
                            serialPort1.Parity = Parity.None;
                            break;
                    }
                    switch (stopbit_cbb.SelectedIndex)
                    {
                        // 1 1.5 2
                        case 0:
                            serialPort1.StopBits = StopBits.One;
                            break;
                        case 1:
                            serialPort1.StopBits = StopBits.OnePointFive;
                            break;
                        case 2:
                            serialPort1.StopBits = StopBits.Two;
                            break;
                        default:
                            serialPort1.StopBits = StopBits.One;
                            break;
                    }

                    serialPort1.Open();
                    isOpen = true;
                    open_btn.Text = "关闭串口";


                }
                else
                {
                    serialPort1.Close();
                    isOpen = false;
                    open_btn.Text = "打开串口";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()+serialPort1.PortName.ToString());
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string dataRecive = serialPort1.ReadExisting();
            recive_rtb.AppendText(dataRecive);
        }
    }
}
