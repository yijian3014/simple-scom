﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace 串口助手
{
    public delegate void TransmitData(byte[] data);

    public delegate void TransmitEventHandler(object sender, TransmitEventArgs e);
    
    public class TransmitEventArgs : EventArgs
    {

        // SerialPort sp { get; set; }
        public byte[] data { get; set; }
    }
}
