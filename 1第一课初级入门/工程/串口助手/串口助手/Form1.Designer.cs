﻿namespace 串口助手
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.port_cbb = new System.Windows.Forms.ComboBox();
            this.send_txb = new System.Windows.Forms.TextBox();
            this.recive_rtb = new System.Windows.Forms.RichTextBox();
            this.send_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "发送框";
            // 
            // port_cbb
            // 
            this.port_cbb.FormattingEnabled = true;
            this.port_cbb.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3"});
            this.port_cbb.Location = new System.Drawing.Point(40, 21);
            this.port_cbb.Name = "port_cbb";
            this.port_cbb.Size = new System.Drawing.Size(121, 21);
            this.port_cbb.TabIndex = 1;
            // 
            // send_txb
            // 
            this.send_txb.Location = new System.Drawing.Point(40, 103);
            this.send_txb.Multiline = true;
            this.send_txb.Name = "send_txb";
            this.send_txb.Size = new System.Drawing.Size(193, 66);
            this.send_txb.TabIndex = 2;
            // 
            // recive_rtb
            // 
            this.recive_rtb.Location = new System.Drawing.Point(317, 103);
            this.recive_rtb.Name = "recive_rtb";
            this.recive_rtb.Size = new System.Drawing.Size(220, 66);
            this.recive_rtb.TabIndex = 3;
            this.recive_rtb.Text = "";
            // 
            // send_btn
            // 
            this.send_btn.Location = new System.Drawing.Point(40, 205);
            this.send_btn.Name = "send_btn";
            this.send_btn.Size = new System.Drawing.Size(75, 23);
            this.send_btn.TabIndex = 4;
            this.send_btn.Text = "发送";
            this.send_btn.UseVisualStyleBackColor = true;
            this.send_btn.Click += new System.EventHandler(this.send_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "接收框";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 260);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.send_btn);
            this.Controls.Add(this.recive_rtb);
            this.Controls.Add(this.send_txb);
            this.Controls.Add(this.port_cbb);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox port_cbb;
        private System.Windows.Forms.TextBox send_txb;
        private System.Windows.Forms.RichTextBox recive_rtb;
        private System.Windows.Forms.Button send_btn;
        private System.Windows.Forms.Label label2;
    }
}

