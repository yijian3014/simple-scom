﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 串口助手
{
    public partial class Form2 : Form
    {
        public TransmitData useForm1Send;
        public event TransmitEventHandler useForm1Send2;
        public Form2()
        {
            InitializeComponent();
        }

        public void ReciveData(byte[] dataTemp)
        {
            string str = Encoding.GetEncoding("gb2312").GetString(dataTemp);
            str = str.Replace("\0", "\\0");
            richTextBox1.AppendText(str);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] dataTemp = Encoding.GetEncoding("gb2312").GetBytes(textBox1.Text);
            useForm1Send?.Invoke(dataTemp);
            useForm1Send2?.Invoke(this, new TransmitEventArgs{ data = dataTemp });
        }

        internal void ReciveData2(object sender, TransmitEventArgs e)
        {
            string str = Encoding.GetEncoding("gb2312").GetString(e.data);
            //str = str.Replace("\0", "\\0");
            //richTextBox1.AppendText(str);
            MessageBox.Show(str);
        }
    }
}
