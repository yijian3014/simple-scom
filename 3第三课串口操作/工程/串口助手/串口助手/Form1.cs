﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;

namespace 串口助手
{
    public partial class Form1 : Form
    {

        //SerialPort
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //在设计页面已经预先添加了COM1 COM2 COM3 
            this.port_cbb.Items.Add("COM4");
            this.port_cbb.Items.Add("COM5");

            // 给发送框添加预设文本
            this.send_rtb.Text = "我是发送框";

            // 给接收框添加预设文本
            this.recive_rtb.Text = "我是接收框";
        }

        private void send_btn_Click(object sender, EventArgs e)
        {
            // 如果发送的数据不为空，则接收
            if(this.send_rtb.Text != "")
            {
                this.recive_rtb.AppendText(this.send_rtb.Text);
            }
            else
            {
                MessageBox.Show("请先输入发送数据！");

            }
        }

        private void open_btn_Click(object sender, EventArgs e)
        {
            try
            {
                if(port_cbb.Text!="")
                {
                    serialPort1.PortName = port_cbb.Text;
                }
                serialPort1.Open();
                if (serialPort1.IsOpen)
                {
                    MessageBox.Show("serialport is open!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()+serialPort1.PortName.ToString());
            }
        }
    }
}
