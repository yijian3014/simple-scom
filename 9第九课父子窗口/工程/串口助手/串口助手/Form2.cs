﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 串口助手
{
    public partial class Form2 : Form
    {
        public TransmitData useForm1Send;

        public Form2()
        {
            InitializeComponent();
        }

        public void ReciveData(byte[] dataTemp)
        {

            // 2, 编码格式的选择
            string str = Encoding.GetEncoding("gb2312").GetString(dataTemp);
            // 3，0x00 -> \0 结束 不会显示
            str = str.Replace("\0", "\\0");

            richTextBox1.AppendText(str);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            useForm1Send?.Invoke(Encoding.GetEncoding("gb2312").GetBytes(textBox1.Text));
        }
    }
}
